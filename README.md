# **Smtp**

Smtp server for outgoing mail.

### **Source**

+ [postfix](https://hub.docker.com/r/juanluisbaptiste/postfix/)

### **MIT License**

This work is licensed under the terms of **[MIT License](https://bitbucket.org/42spixel/smtp/src/master/LICENSE.md)**.
